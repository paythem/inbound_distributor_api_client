<?php
/**
 * Example client implementation.
 */

require_once __DIR__ . '/vendor/autoload.php';

use Paythem\ClientIntegration\Distributor\API;

try {
	$api                            = new API(
		'demo',
		49972,
		'',
		'',
		'',
		''

	);

	$res					        = $api->topup_listRetailers();
//	$res					        = $api->topup_Retailer(
//		11229,                  // Retailer ID as returned by topup_listRetailers
//		random_int(1,9999999),  // Only for testing purposes, prefer using GUID
//		50                      // Amount to topup with.
//	);
	
	var_export($api);                               // Dump full API class
	var_dump($res);                                 // Dump the response. Equal to $api->response.
	var_export($api->response);                     // Response array, with SERVER_TRANSACTION_ID, SYSLOG_ID, RESULT code, ERROR_DESCRIPTION and CONTENT
	var_export($api->result);                       // Only the result ($api->response['CONTENT']) value. Contains "ERROR" if error occurred.
	var_export($api->errorDescription);             // Description of any error that occurred.
	var_export($api->ERROR);                        // Boolean error flag. True if error occurred.

} catch (Throwable $e) {
	var_dump($e->getMessage());
}