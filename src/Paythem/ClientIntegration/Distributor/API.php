<?php

namespace Paythem\ClientIntegration\Distributor;

use Exception;
use JsonException;
use Paythem\ClientIntegration\baseIntegration;

/**
 * This class is used for direct integration with the PayThem.Net Electronic Voucher Distribution API subsystem.
 *
 * This is the base class. Any calls to this must follow the API protocol inherently, without deviation.
 *
 * @contact support@paythem.atlassian.net
 * @version 1.0.0
 * @copyright PayThem.Net WLL, 2014-2024
 *
 * @requires Requires openSSL.
 */

class API
	extends baseIntegration {

	/**
	 * @var string $CLIENT_VERSION Current client version.
	 */
	protected string $CLIENT_VERSION                 = '7.7.1';
	
	/**
	 * List all retailers that are top-up enabled, including the maximum amount to be topped up with.
	 *
	 * @return array|null
	 * @throws JsonException
	 * @throws Exception
	 */
	public function topup_listRetailers(): ?array {
		$this->innerVars['FUNCTION']                = __FUNCTION__;

		return $this->callAPI();
	}

	/**
	 * Top-up a selected, top-up enabled retailer wallet with a value. The maximum value is defined server side and
	 * returned with listRetailers.
	 *
	 * @param int    $retailerID  Retailer ID that is to be topped up. As returned by topup_listRetailers.
	 * @param float  $topupAmount The amount to add to the retailer's wallet.
	 * @param string $reference
	 *
	 * @return array|null
	 * @throws JsonException
	 * @throws Exception
	 */
	public function topup_Retailer(int $retailerID, string $reference, float $topupAmount): ?array {
		if (strlen(trim($reference)) < 6) {
			throw new Exception('Reference needs to be at least 6 long.');
		}

		$this->innerVars['FUNCTION']                = __FUNCTION__;
		$this->innerVars['PARAMETERS']              = [
			'RETAILER_ID'                           => $retailerID,
			'REFERENCE'                             => $reference,
			'AMOUNT'                                => round($topupAmount, 2),
		];

		return $this->callAPI();
	}
	
	/**
	 * Retrieve a list of top-up transactions within the date range requested.
	 *
	 * @param string $fromDate From date of the transaction listing. CCYY-MM-DD format, as per documentation.
	 * @param string $toDate   To date of the transaction listing. CCYY-MM-DD format, as per documentation
	 *
	 * @return mixed
	 * @throws JsonException
	 * @throws Exception
	 * @deprecated UNIMPLEMENTED.
	 */
	public function topup_listTransactions(string $fromDate, string $toDate): mixed {
		throw new Exception(__FUNCTION__.': Not implemented yet.');

		$this->innerVars['FUNCTION']                = __FUNCTION__;
		$this->innerVars['PARAMETERS']              = [
			'FUNCTION'                              => __FUNCTION__,
			'FROM_DATE'                             => $fromDate,
			'TO_DATE'                               => $toDate,
		];
		
		return $this->callAPI();
	}
	
	/**
	 * Returns the general status of all top-ups occurred during the day compared to the limit for the distributor.
	 *
	 * @return mixed
	 * @throws JsonException
	 * @throws Exception
	 * @deprecated UNIMPLEMENTED.
	 */
	public function topup_generalStatus(): mixed {
		throw new Exception(__FUNCTION__.': Not implemented yet.');
		$this->innerVars['FUNCTION']                = __FUNCTION__;

		return $this->callAPI();
	}
	
	/**
	 * Returns the status of a top-up transaction for a specific retailer.
	 *
	 * @param int $serverTransactionID The transaction ID, as returned by VVS in topup_Retailer.
	 *
	 * @return mixed
	 * @throws JsonException
	 * @throws Exception
	 * @deprecated UNIMPLEMENTED.
	 */
	public function topup_transactionStatusByID(int $serverTransactionID): mixed {
		throw new Exception(__FUNCTION__.': Not implemented yet.');

		$this->innerVars['FUNCTION']                = __FUNCTION__;
		$this->innerVars['PARAMETERS']              = [
			'TRANSACTION_ID'                        => $serverTransactionID,
		];
		
		return $this->callAPI();
	}
	
	/**
	 * Returns the status of a top-up transaction for a specific retailer, by client reference.
	 *
	 * @param string $reference The client transaction reference, as sent to VVS during topup_Retailer.
	 *
	 * @return mixed
	 * @throws JsonException
	 * @throws Exception
	 * @deprecated UNIMPLEMENTED.
	 */
	public function topup_transactionStatusByReference(string $reference): mixed {
		throw new Exception(__FUNCTION__.': Not implemented yet.');

		$this->innerVars['FUNCTION']                = __FUNCTION__;
		$this->innerVars['PARAMETERS']              = [
			'REFERENCE'                             => $reference,
		];

		return $this->callAPI();
	}
	
	/**
	 * Returns the status (on daily transaction limits) for a specific retailer.
	 *
	 * @param int $retailerID The ID of the retailer to check the daily status for.
	 *
	 * @return mixed
	 * @throws JsonException
	 * @throws Exception
	 * @deprecated UNIMPLEMENTED.
	 */
	public function topup_retailerStatus(int $retailerID): mixed {
		throw new Exception(__FUNCTION__.': Not implemented yet.');

		$this->innerVars['FUNCTION']                = __FUNCTION__;
		$this->innerVars['PARAMETERS']              = [
			'TRANSACTION_ID'                        => $retailerID,
		];

		return $this->callAPI();
	}
}